document.addEventListener("DOMContentLoaded", () => {
  const bioBtn = document.querySelector("#bio-btn");
  const bioContainer = document.querySelector("#bio");
  const header = document.querySelector("header");
  const caption = document.querySelector("figcaption");
  const projectTitleContainer = document.querySelector("#project__title");
  const projectSubtitleContainer = document.querySelector("#project__subtitle");
  const projectTitle = caption.dataset.title;
  const projectSubtitle = caption.dataset.subtitle;
  const projectContainer = document.querySelector("#project");
  const frontImage = document.querySelector("#project__front");
  const backgroundImageContainer = document.querySelector("#project figure");
  const backgroundImage = document.querySelector("#project__background");

  header.addEventListener("click", toggleBio);

  // caption.addEventListener("mouseover", showNextProjectLabel);
  // caption.addEventListener("mouseleave", hideNextProjectLabel);

  function toggleBio(e) {
    if (e.target.closest("a")) return;
    header.classList.toggle("expanded");
    bioContainer.classList.toggle("hide");
  }

  changeImagesOnClick();

  if (window.innerWidth < 700) {
    setBackgroundImage();
    setFrontImage();
    updateImagesArray();
  }

  hideFrontImageOnMouseOut();
  linkCursor();
  frontImage.setAttribute("src", images[0]["src"]);
  // needs srcset in helper and config
  // frontImage.setAttribute("srcset", images[0]["srcset"]);
  frontImage.setAttribute("alt", images[0]["alt"]);

  projectContainer.addEventListener("mouseover", () => {
    hideFrontImageOnMouseOut();
    linkCursor();
    frontImage.setAttribute("src", images[0]["src"]);
    // needs srcset in helper and config
    // frontImage.setAttribute("srcset", images[0]["srcset"]);
    frontImage.setAttribute("alt", images[0]["alt"]);
  });

  function showFrontImage() {
    frontImage.classList.remove("hide");
  }

  function hideFrontImageOnMouseOut() {
    projectContainer.addEventListener("mouseout", () => {
      frontImage.classList.add("hide");
    });
  }

  function linkCursor() {
    const windowMiddleX = window.innerWidth / 2;
    const windowMiddleY = window.innerHeight / 2;

    projectContainer.addEventListener(
      "mousemove",
      throttle((e) => {
        lastMouseEvent = e;
        requestAnimationFrame(() => {
          if (lastMouseEvent === e) {
            linkFrontImageToCursor(e);
            linkBackgroundBlurToCursor(e, windowMiddleX, windowMiddleY);
          }
        });
      }, 5)
    );
  }

  function linkFrontImageToCursor(e) {
    if (!e) return;
    showFrontImage();

    let x = e.x - frontImage.offsetWidth / 2;
    let y = e.y - frontImage.offsetHeight / 2;

    frontImage.style.top = y + "px";
    frontImage.style.left = x + "px";
  }

  function linkBackgroundBlurToCursor(e, windowMiddleX, windowMiddleY) {
    if (!e) return;
    const distFromCenterX = Math.abs((windowMiddleX - e.x) / 30);
    const distFromCenterY = Math.abs((windowMiddleY - e.y) / 30);

    const medianDistFromCenter = (distFromCenterX + distFromCenterY) / 2;
    backgroundImage.style.filter = `blur(${medianDistFromCenter}px)`;
  }

  function changeImagesOnClick() {
    const clickHandler = (e) => {
      setBackgroundImage(e);
      setFrontImage(e);
      updateImagesArray();
    };

    projectContainer.removeEventListener("click", clickHandler);
    projectContainer.addEventListener("click", clickHandler);
  }

  function setBackgroundImage(e) {
    if (window.innerWidth > 640) {
      backgroundImage.classList.add("hide");
      frontImage.addEventListener("load", () => {
        backgroundImage.classList.remove("hide");
      });
      const x = e.clientX - frontImage.offsetWidth / 2;
      const y = e.clientY - frontImage.offsetHeight / 2;
      backgroundImageContainer.style.left = x + "px";
      backgroundImageContainer.style.top = y + "px";
    }
    backgroundImage.setAttribute("src", images[0]["src"]);
    // needs srcset in helper and config
    // backgroundImage.setAttribute("srcset", images[0]["srcset"]);
    backgroundImage.setAttribute("alt", images[0]["alt"]);
  }

  function setFrontImage(e) {
    frontImage.setAttribute("src", images[1]["src"]);
    // needs srcset in helper and config
    // frontImage.setAttribute("srcset", images[1]["srcset"]);
    frontImage.setAttribute("alt", images[1]["alt"]);
    frontImage.classList.add("hide");
    frontImage.addEventListener("load", () => {
      linkFrontImageToCursor(e);
    });
    preloadImage(images[2]["src"]);
  }

  function preloadImage(url) {
    const img = new Image();
    img.src = url;
    console.log("preload :", url);
  }

  function updateImagesArray() {
    images.push(images.shift());
  }

  function showNextProjectLabel() {
    if (window.innerWidth < 640) return;
    caption.style.minHeight = caption.offsetHeight + "px";
    caption.style.display = "flex";
    caption.style.alignItems = "flex-end";
    projectTitleContainer.textContent = "Next project";
    projectSubtitleContainer.textContent = " →";
  }

  function hideNextProjectLabel() {
    if (window.innerWidth < 640) return;
    caption.style.minHeight = "";
    caption.style.display = "";
    caption.style.alignItems = "";
    projectTitleContainer.textContent = projectTitle;
    projectSubtitleContainer.textContent = projectSubtitle;
  }
});

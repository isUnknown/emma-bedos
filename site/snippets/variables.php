<script>
  
  <?php $files = resizeMultipleFilesByWidth($page->files()->sorted()); ?>
  
  const images = <?= json_encode($files) ?>;

</script>
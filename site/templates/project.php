<!-- Coded by Adrien Payet -->
<!-- adrien.payet@outlook.com -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <link rel="stylesheet" href="<?= url('assets') ?>/style.css">
  <script src="<?= url('assets') ?>/lodash-throttle.js" defer></script>
  <script src="<?= url('assets') ?>/script.js" defer></script>

  <?php if ($site->favicon()->isNotEmpty()): ?>
      <link rel="icon" type="image/x-icon" href="<?= $site->favicon()->toFile()->url() ?>">
    <?php endif ?>

  <title><?= $site->title() ?> - <?= $page->title() ?></title>
  <meta name="description" content="<?= $site->description() ?>">

  <meta property='og:title' content='<?= $site->title() ?>' />
  <meta property='og:type' content='website' />
  <meta property='og:url' content='<?= $site->url() ?>' />
  <?php if ($site->socialImg()->isNotEmpty()): ?>
    <meta property='og:image' content='<?= $site->socialImg()->toFile()->url() ?>'>
  <?php endif ?>
  <meta property='og:description' content='<?= $site->description() ?>' />
</head>
<body>
  <header>
    <button id="bio-btn"><h1>Emma Bedos</h1></button>
    <div id="bio" class="hide">
      <?= $site->bio()->kt() ?>
    </div>
    <footer>
      <a target="_blank" href="mailto:ema.bedos@hotmail.fr"><h2><?= $site->email() ?></h2></a>
      <a target="_blank" href="https://www.instagram.com/<?=$site->instagram() ?>"><h2>@<?= $site->instagram() ?></h2></a>
    </footer>
  </header>
  <div id="project">
    <figure>
      <img id="project__background" src="" alt="">
      <figcaption data-title="<?= $page->title() ?>" data-subtitle=", <?= $page->subtitle() ?>">
        <div id="next">
          <a 
            title="<?php e($page->hasNextListed(), 'Next project → ' . $page->next()->title(), 'Next project → ' . $site->children()->listed()->first()->title()) ?>" 
            href="<?php e($page->hasNextListed(), $page->next()->url(), $site->children()->listed()->first()->url()) ?>"
          >Next project →</a>
        </div>
        <!-- <a 
          title="<?php e($page->hasNextListed(), 'Next project → ' . $page->next()->title(), 'Next project → ' . $site->children()->listed()->first()->title()) ?>" 
          href="<?php e($page->hasNextListed(), $page->next()->url(), $site->children()->listed()->first()->url()) ?>"
        > -->
        <h2 id="project__title" class="underline"><?= $page->title() ?></h2>
      <!-- </a> -->
      <span id="project__subtitle">, <?= $page->subtitle() ?></span>
      </figcaption>
    </figure>
    <img id="project__front" class="hide" alt="">
  </div>
  <a class="arrow" href="<?php e($page->hasNextListed(), $page->next()->url(), $site->children()->listed()->first()->url()) ?>">→</a>
  <?php snippet('variables') ?>
</body>
</html>
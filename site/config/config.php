<?php

return [
  'debug' => true,
  'medienbaecker.autoresize.maxWidth' => 1600,
  'medienbaecker.autoresize.maxHeight' => 1000,
  // needs srcset in helper and script
  // 'thumbs' => [
  //   'srcsets' => [
  //     'default' => [
  //       '800w' => ['width' => 640],
  //       '1024w' => ['width' => 820],
  //       '1440w' => ['width' => 1150],
  //       '2048w' => ['width' => 1600]
  //     ]
  //   ]
  // ]
];
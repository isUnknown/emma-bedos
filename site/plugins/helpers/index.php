<?php

function resizeMultipleFilesByWidth($files, $width = 1000) {
  $resizedFiles = [];
  foreach ($files as $file) {
    $dataFile = [
      'alt' => $file->alt()->isNotEmpty() ? $file->alt()->value() : preg_replace('/(\.[a-z, A-Z]+)/', '', $file->filename()),
      // needs srcset in config and script
      // 'srcset' => $file->srcset(),
    ];
    
    // RESIZE CODE DISABLED BECAUSE IT ALTERED THE IMAGE COLORS
    // if ($file->width() > 1600) {
    //   $dataFile['src'] = $file->resize(1600)->size() > 500000 ? $file->resize(1600, null, 80)->url() : $file->resize(1600)->url();
    // } elseif ($file->height() > 1000) {
    //   $dataFile['src'] = $file->resize(null, 1000)->size() > 500000 ? $file->resize(null, 1000, 80)->url() : $file->resize(null, 1000)->url();
    // } else {
    //   $dataFile['src'] = $file->size() > 500000 ? $file->quality(80)->url() : $file->url();
    // }

    $dataFile['src'] = $file->url();
    $resizedFiles[] = $dataFile;
  }
  return $resizedFiles;
}
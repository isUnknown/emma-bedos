# Site d'Emma Bedos

https://emma-bedos.com

## Administration du site

https://emma-bedos.com/panel

## Basé sur le CMS Kirby

Site du CMS : https://getkirby.com/

### Mettre à jour le CMS

Lancer la commande :

```
composer update getkirby/cms
```

Les mises à jour majeur peuvent nécessiter des mises à jour du template des projets (`site/templates/project`).
